class Drive {
	constructor(name, zeroPage) {
		this.name = name;
		this.zeroPage = zeroPage;
		this.zeroAuth = new ZeroAuth(zeroPage);
		this.zeroFS = new ZeroFS(zeroPage);

		this.userContent = {
			"files": {},
			"signs": {},
			"ignore": "drives/.*"
		};
	}

	getAuth() {
		return this.zeroAuth.getAuthAsync()
			.then(auth => {
				if(auth) {
					return auth;
				}

				return this.zeroAuth.requestAuth();
			});
	}

	saveUserData() {
		let auth;

		return this.getAuth()
			.then(a => {
				auth = a;
				return this.zeroFS.fileExists("data/users/" + auth.address + "/content.json");
			})
			.then(exists => {
				if(!exists) {
					return this.zeroFS.writeFile("data/users/" + auth.address + "/content.json", JSON.stringify(this.userContent));
				}
			})
			.then(() => {
				this.zeroPage.publish("data/users/" + auth.address + "/content.json");
			});
	}

	doesExist() {
		return this.getAuth()
			.then(auth => {
				return this.zeroFS.fileExists("data/users/" + auth.address + "/drives/" + this.name + ".drv");
			});
	}
	create(from="drive/empty.drv", buf=null) {
		let empty;
		return Promise.resolve()
			.then(() => {
				if(buf) {
					return buf;
				} else {
					return this.zeroFS.readFile(from, "arraybuffer");
				}
			})
			.then(e => {
				empty = e;
				return this.getAuth();
			})
			.then(auth => {
				return this.zeroFS.writeFile("data/users/" + auth.address + "/drives/" + this.name + ".drv", empty, "arraybuffer");
			})
			.then(() => {
				return this.saveUserData();
			});
	}

	peek(offset, length) {
		return this.getAuth()
			.then(auth => {
				return this.zeroFS.peekFile("data/users/" + auth.address + "/drives/" + this.name + ".drv", offset, length, "arraybuffer");
			});
	}

	readHeader() {
		return this.peek(0, 12);
	}


	search(from, value) {
		let buffer = [];

		const handlePart = from => {
			return this.peek(from, 1024)
				.then(res => {
					if(res.length == 0) {
						return null;
					}

					buffer = buffer.concat(Array.from(res));

					let pos = util.contains(buffer, value);
					if(pos !== null) {
						return pos;
					} else {
						return handlePart(from + 1024);
					}
				});
		};

		return handlePart(from);
	}
};