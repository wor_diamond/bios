# Drive

## Extension

`.drv` (DRiVe).

## Drive format

*Size*              *Comment*                           *Example*
4 bytes             Signature                           `WDRV`
4 bytes             Address of loader (absolute)        `00 00 00 00`
4 bytes             Address of root file (absolute)     `00 00 00 F0`
unknown             Files

## File format

*Size*              *Comment*                           *Example*
1 byte              File type                           `00`
1 byte              Flags for owner                     `07`
1 byte              Flags for owner's group             `07`
1 byte              Flags for everyone else             `05`
4 bytes             Fragment size                       `00 00 00 FF`
4 bytes             Address of the next files of the    `00 00 00 00`
                    file (fragmentation)
32 bytes            Owner                               `root 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00`
unknown             File content (may occupy more space `Hello world!`
                    then `File size`, only first `File`
                    `size` bytes will be read)

## File types

*Name*              *Code*                              *Content*
Regular file        `00`                                The contents of the file
Directory           `01`                                See **Directory format**
Link                `02`                                Address of the linked file

## File flags

*Name*              *Abbreviation*                      *Bit*
Read                `r`                                 0
Write               `w`                                 1
Execute             `x`                                 2

## Directory format

*Size*              *Comment*                           *Example*
4 bytes             File count                          `00 00 00 02`

**For each file:**
    unknown         File name                           `test`
    1 byte          Null separator                      `00`
    4 bytes         Address                             `00 43 C2 F8`