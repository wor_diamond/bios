class BIOS {
	constructor(zeroPage) {
		this.zeroPage = zeroPage;
		this.zeroFS = new ZeroFS(zeroPage);

		this.flashPromise = null;
		this.flash = null;
		window.onkeydown = e => {
			if(e.key == "Shift") {
				if(this.flashPromise) {
					return;
				}

				this.setStatus("Insert flash");

				this.flashPromise = this.getPrivateNamespace()
					.then(ns => this.insertFlash(ns));
			}
		};

		setTimeout(() => {
			window.onkeydown = null;

			this.searchForDrive()
				.catch(e => {
					console.error(e);
					this.setStatus(e.message || e);
				});
		}, 2000);
	}

	$(query) {
		return document.querySelector(query);
	}
	setStatus(status) {
		this.$(".bios-status").textContent = status;
	}

	searchForDrive() {
		this.setStatus("Searching for drive");

		return this.getPrivateNamespace()
			.then(ns => {
				this.drive = new Drive(ns + "/root", this.zeroPage);

				if(this.flashPromise) {
					this.setStatus("Insert flash");
					return this.flashPromise;
				} else {
					return null;
				}
			})
			.then(() => {
				return this.loadFrom(this.flash || this.drive);
			});
	}
	loadFrom(drive) {
		return drive.doesExist()
			.then(exists => {
				if(!exists) {
					return this.installKernel();
				}
			})
			.then(() => {
				return this.bootFromDrive(drive);
			});
	}

	insertFlash(ns) {
		return new Promise((resolve, reject) => {
			let input = document.createElement("input");
			input.type = "file";
			input.style.position = "absolute";
			input.onchange = () => {
				if(input.files.length == 0) {
					reject(new Error("No flash image"));
					return;
				}

				resolve(input.files[0]);
				input.parentNode.removeChild(input);
			};
			document.body.appendChild(input);
		})
			.then(file => {
				return new Promise((resolve, reject) => {
					let reader = new FileReader();
					reader.onload = () => {
						resolve(reader.result);
					};
					reader.onerror = e => {
						reject(e);
					};
					reader.readAsArrayBuffer(file);
				});
			})
			.then(buf => {
				this.flash = new Drive(ns + "/flash", this.zeroPage);
				return this.flash.create(null, buf);
			})
			.then(() => this.flash)

			.catch(e => {
				if(e && e.message == "No flash image") {
					return null;
				}
				throw e;
			});
	}

	installKernel() {
		this.setStatus("Installing kernel");

		return this.drive.create("drive/kernel.drv");
	}

	bootFromDrive(drive) {
		this.setStatus("Booting from <" + drive.name + ">")

		let loaderOffset;
		return drive.readHeader()
			.then(header => {
				let signature = util.fromString(util.slice(header, 0, 4));
				if(signature != "WDRV") {
					throw new Error("SIGNATURE MISMATCH");
				}

				let loader = util.fromUint32(util.slice(header, 4, 4));
				if(loader == 0) {
					throw new Error("NO LOADER PRESENT");
				}

				loaderOffset = loader;

				return drive.search(loader, util.toUint32(0xDEADBEEF));
			})
			.then(length => {
				if(length === null) {
					throw new Error("NO 0xDEADBEEF PRESENT");
				}

				return drive.peek(loaderOffset, length);
			})
			.then(loaderData => {
				let code = util.fromString(loaderData);

				try {
					code = new Function("drive, bios", code);
				} catch(e) {
					if(e instanceof SyntaxError) {
						console.error(e);
						throw new Error("MALFORMED LOADER");
					}
					throw e;
				}

				this.setStatus("Executing");
				code(drive, this);
			});
	}

	getPrivateNamespace() {
		return this.zeroPage.cmd("userGetSettings")
			.then(settings => {
				if(settings.privateNamespace) {
					return settings.privateNamespace;
				}

				settings.privateNamespace = Math.random().toString(16).substr(2);

				return this.zeroPage.cmd("userSetSettings", [settings])
					.then(() => settings.privateNamespace);
			});
	}
};