window.util = {
	to(num, cnt) {
		let res = [];
		for(let i = 0; i < cnt; i++) {
			res.push(num % 0x100);
			num = Math.floor(num / (1 << 8));
		}
		return new Uint8Array(res);
	},
	from(buf, cnt) {
		let res = 0;
		for(let i = buf.length - 1; i >= 0; i--) {
			res *= 1 << 8;
			res += buf[i];
		}
		return res;
	},

	toUint32(num) {
		return this.to(num, 4);
	},

	fromUint32(buf) {
		return this.from(buf, 4);
	},
	fromString(buf) {
		let bytes = Array.from(buf).map(chr => String.fromCharCode(chr)).join("");
		let utf8 = decodeURIComponent(escape(bytes));

		return utf8;
	},

	slice(buf, offset, length) {
		return buf.slice(offset, offset + length);
	},

	contains(buf, arr) {
		for(let i = 0; i <= (buf.length || buf.byteLength || 0) - (arr.length || arr.byteLength || 0); i++) {
			let cur = this.slice(buf, i, arr.length || arr.byteLength || 0);
			if(cur.join(",") == arr.join(",")) {
				return i;
			}
		}
		return null;
	}
};